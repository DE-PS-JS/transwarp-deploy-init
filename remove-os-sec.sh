#!/bin/bash
echo "-----------------start-----------------"
echo "stopping firewall........"
systemctl stop SuSEfirewall.service

echo "disabling firewall........"
systemctl disable SuSEfirewall.service

echo "getting rpm-firewall names........"
rpm -qa | grep firewall >> rpm.txt

echo "removing firewall........"
for line in $(cat rpm.txt)
do
 zypper remove $line
done
rm -rf rpm.txt
echo "removing apparmor......"
zypper remove apparmor* yast2-apparmor

echo "-----------------finish-----------------"
