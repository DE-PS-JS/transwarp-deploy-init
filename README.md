# TDH Installation Initialization

---
## 一、使用背景：
​	为了清理并规范化安装系统之后的环境以及配合铺垫安装TDH Manager，需要按照对应需求使用该脚本。

---
## 二、脚本概述：
1. #####   初始化总脚本：TDH-install-init.sh
2. #####   倒计时：countdown.sh
3. #####   格式化挂载数据盘：bulk-format-mount.sh
4. #####   校正时间：adjust-tz-dst.sh
5. #####   移除系统安全模块：remove-os-sec.sh
6. #####   挂载ftp镜像：mount-iso.sh
7. #####   测试时间：test-date.sh
8. #####   复制os文件：cp-os.sh

---
## 三、使用方法：
​	打开TDH-install-init.sh，根据文字选择所选项选择所需操作，并在结束后选择退出。