#!/bin/bash
echo "-----------------start-----------------"
echo 'show current system time'
date

echo 'rm /etc/localtime'
rm -rf /etc/localtime

echo 'show current UTC time'
date

echo 'link localtime to Etc/GMT-8'
ln /usr/share/zoneinfo/Etc/GMT-8 -sf /etc/localtime

echo 'sync time from NTP server'
sntp -r 115.28.122.198

echo 'show current system time'
date

echo 'adjust and unify /etc/sysconfig/clock'
echo 'set HWCLOCK to "--localtime"'
sed -i 's/HWCLOCK=.*/HWCLOCK="--localtime"/g' /etc/sysconfig/clock

echo 'set SYSTOHC to "yes"'
sed -i 's/SYSTOHC=.*/SYSTOHC="yes"/g' /etc/sysconfig/clock

echo 'set TIMEZONE and DEFAULT_TIMEZONE to "Asia/Beijing"'
sed -i 's/TIMEZONE=.*/TIMEZONE="Asia\/Beijing"/g' /etc/sysconfig/clock

echo 'set system time back to hardware clock'
hwclock -w

echo 'show hwclock and hwclock --localtime'
hwclock && hwclock --localtime


echo "Please choose 1.已安装java 2.未安装java"
read number
if [ ${number} -eq 1 ]; then
	echo 'check timezone'
	java -cp ./test-date-1.0.0.jar io.transwarp.test.TestDate
elif [ ${number} -eq 2 ]; then
        exit 0
else
        echo "Wrong number"
        exit 1
fi

echo "-----------------finish-----------------"

