#!/bin/bash
echo "-----------------start-----------------"
cp /etc/fstab /root
echo "/etc/fstab backed up to /root"

#sed -i "/docker/d" /etc/fstab
#echo -e "Docker item removed from /etc/fstab if exists\n\n"


# Formatting all data storages without patition them
# Suppose that OS installed in /dev/sda, and /dev/sd[b-z] are disks for data storages.

echo "Linux SUSE11 磁盘文件系统为EXT3 SUSE12 磁盘文件系统为EXT4" 
echo "Please choose 1.Ext4 2.Ext3"
read number
#echo ${number}
if [ ${number} -eq 1 ]; then
    ext="ext4"
    #echo ${ext}
elif [ ${number} -eq 2 ]; then
    ext="ext3"
    #echo ${ext}
else
    echo "Wrong number"
    exit 1
fi

for sd in /dev/sd[b-z]; do
    umount $sd
    echo "\nUmount $sd for retries"

    disk=${sd##*/}
    sed -i "/$disk/d" /etc/fstab
    echo "Old item of $sd deleted from /etc/fstab for retries"

    echo -e "--!!!!-- \033[31m starting to format $sd \033[0m"
    echo -ne '\E[1;0m'

    mkfs.${ext} -F -N 20000000 "$sd"
    echo "$sd formatted"

    dir=/mnt/${sd##*/}
    mkdir -p "$dir"
    echo "$dir created"

    item=""$sd"     "$dir"      ${ext}    defaults,noatime    0   0"
    echo "$item" >> /etc/fstab
    echo "$item added to /etc/fstab"

    echo -e "---- $sd finished ----\n"
done

mount -a
echo "All data storages mounted"
echo "-----------------finish-----------------"

