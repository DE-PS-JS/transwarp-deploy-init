#!/bin/bash
echo "Please choose: 
安装Manager之前：
        1.格式化挂载数据盘 
        2.校正时间 
        3.移除系统安全模块 
        4.挂载ftp镜像
安装Manager之后：
        5.测试时间 
        6.复制os文件 
完成安装：
        7.退出"
read number
#echo ${number}
if [ ${number} -eq 1 ]; then
    ./countdown.sh
    ./bulk-format-mount.sh
    ./deploy-init.sh
elif [ ${number} -eq 2 ]; then
    ./countdown.sh
    ./adjust-tz-dst.sh
    ./deploy-init.sh
elif [ ${number} -eq 3 ]; then
    ./countdown.sh
    ./remove-os-sec.sh
    ./deploy-init.sh
elif [ ${number} -eq 4 ]; then
    ./countdown.sh
    ./mount-iso.sh
    ./deploy-init.sh
elif [ ${number} -eq 5 ]; then
    ./countdown.sh
    ./test-date.sh
    ./deploy-init.sh
elif [ ${number} -eq 6 ]; then
    ./countdown.sh
    ./cp-os.sh
    ./deploy-init.sh
elif [ ${number} -eq 7 ]; then
        echo "-----------------finish-----------------"
    exit 1
else
    echo "Wrong number,back"
    ./deploy-init.sh
fi

